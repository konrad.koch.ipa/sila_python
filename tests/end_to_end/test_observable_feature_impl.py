import datetime
import time
from typing import Optional
from uuid import UUID

import pytest

from sila2.client.client_observable_command_instance import ClientObservableCommandInstance
from sila2.client.sila_client import SilaClient
from sila2.framework.errors.command_execution_not_finished import CommandExecutionNotFinished
from sila2.server.sila_server import SilaServer
from tests.end_to_end.timer_impl import Timer, TimerImpl
from tests.utils import generate_port


def test():
    class TestServer(SilaServer):
        def __init__(self):
            super().__init__(
                server_name="TestServer",
                server_type="TestServer",
                server_version="0.1",
                server_description="A test SiLA2 server",
                server_vendor_url="https://gitlab.com/sila2/sila_python",
            )

            self.timer = TimerImpl(self.child_task_executor)

            self.set_feature_implementation(Timer, self.timer)

        def stop(self, grace_period: Optional[float] = None):
            self.timer.stop()
            super().stop(grace_period)

    server = TestServer()
    port = generate_port()
    address = "127.0.0.1"

    try:
        server.start_insecure(address, port, enable_discovery=False)
        client = SilaClient(address, port)

        countdown_instance_1 = client.Timer.Countdown(3)
        countdown_instance_2 = client.Timer.Countdown(5)

        assert isinstance(countdown_instance_1, ClientObservableCommandInstance)
        assert isinstance(countdown_instance_2, ClientObservableCommandInstance)

        assert isinstance(countdown_instance_1.execution_uuid, UUID)
        assert countdown_instance_1.execution_uuid != countdown_instance_2.execution_uuid

        with pytest.raises(CommandExecutionNotFinished):
            countdown_instance_1.get_responses()

        assert countdown_instance_1.done is False
        print(list(countdown_instance_1.subscribe_intermediate_responses()))
        time.sleep(0.1)  # avoid race condition between intermediate response update and execution info update
        assert countdown_instance_1.done is True

        assert isinstance(countdown_instance_1.get_responses().Timestamp, datetime.datetime)

        assert isinstance(client.Timer.CurrentTime.get(), datetime.datetime)

        times = client.Timer.CurrentTime.subscribe()
        assert isinstance(next(times), datetime.datetime)
        times.cancel()

    finally:
        server.stop()
