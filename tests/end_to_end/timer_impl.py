from __future__ import annotations

import datetime
import time
from concurrent.futures import Executor
from os.path import abspath, dirname, join
from queue import Queue
from threading import Event
from typing import Any, Dict

from sila2.framework.command.execution_info import CommandExecutionStatus
from sila2.framework.feature import Feature
from sila2.framework.fully_qualified_identifier import FullyQualifiedIdentifier
from sila2.server import ObservableCommandInstanceWithIntermediateResponses
from sila2.server.feature_implementation_base import FeatureImplementationBase
from tests.utils import get_feature_definition_str

root_dir = join(abspath(dirname(__file__)), "..")

Timer = Feature(get_feature_definition_str("Timer"))


class TimerImpl(FeatureImplementationBase):
    _CurrentTime_producer_queue: Queue[datetime.datetime]

    def __init__(self, executor: Executor):
        self._CurrentTime_producer_queue = Queue()
        self.__stop_event = Event()

        def update_time(stop_event: Event):
            while not stop_event.is_set():
                self._CurrentTime_producer_queue.put(datetime.datetime.now(tz=datetime.timezone.utc))
                time.sleep(1)

        executor.submit(update_time, self.__stop_event)

    def Countdown(
        self,
        N: int,
        *,
        metadata: Dict[FullyQualifiedIdentifier, Any],
        instance: ObservableCommandInstanceWithIntermediateResponses[int],
    ) -> datetime.datetime:
        instance.status = CommandExecutionStatus.running
        instance.progress = 0
        for i in range(N, 0, -1):
            instance.send_intermediate_response(i)
            instance.progress = (N - i) / N * 100
            time.sleep(1)

        return datetime.datetime.now(tz=datetime.timezone.utc)

    def CurrentTime_on_subscription(self, *, metadata: Dict[FullyQualifiedIdentifier, Any]) -> None:
        # optional method
        pass

    def stop(self):
        self.__stop_event.set()
