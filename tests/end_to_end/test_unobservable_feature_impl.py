from sila2.client.sila_client import SilaClient
from sila2.server.sila_server import SilaServer
from tests.end_to_end.greetingprovider_impl import GreetingProvider, GreetingProviderImpl
from tests.utils import generate_port


def test():
    class TestServer(SilaServer):
        def __init__(self):
            super().__init__(
                server_name="TestServer",
                server_type="TestServer",
                server_version="0.1",
                server_description="A test SiLA2 server",
                server_vendor_url="https://gitlab.com/sila2/sila_python",
            )

            self.set_feature_implementation(GreetingProvider, GreetingProviderImpl())

    server = TestServer()
    port = generate_port()
    address = "127.0.0.1"

    try:
        server.start_insecure(address, port, enable_discovery=False)
        client = SilaClient(address, port)

        assert client.GreetingProvider.SayHello("World").Greeting == "Hello World"
        assert client.GreetingProvider.StartYear.get() == 2021

    finally:
        server.stop()
