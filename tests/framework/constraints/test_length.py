import pytest

from sila2.framework.constraints.length import Length


def test():
    length3 = Length(3)
    assert length3.validate("abc")
    assert length3.validate(b"abc")
    assert not length3.validate("abcd")

    assert repr(length3) == "Length(3)"

    _ = Length(2 ** 63 - 1)
    with pytest.raises(ValueError):
        _ = Length(-1)
    with pytest.raises(ValueError):
        _ = Length(2 ** 63)
