from sila2.framework.constraints.minimal_element_count import MinimalElementCount


def test():
    c = MinimalElementCount(3)

    assert repr(c) == "MinimalElementCount(3)"

    assert not c.validate([1, 2])
    assert c.validate([1, 2, 3])
    assert c.validate([1, 2, 3, 4])
