from datetime import datetime, timedelta, timezone

import pytest


def test_to_native_type(basic_feature):
    timestamp_field = basic_feature._data_type_definitions["Datetime"].data_type
    SilaTimestamp = timestamp_field.message_type
    SilaTimezone = basic_feature._pb2_module.SiLAFramework__pb2.Timezone

    msg = SilaTimestamp(
        year=2000,
        month=6,
        day=5,
        hour=1,
        minute=2,
        second=3,
        timezone=SilaTimezone(hours=7, minutes=30),
    )
    t = timestamp_field.to_native_type(msg)

    assert isinstance(t, datetime)
    assert t.year == 2000
    assert t.month == 6
    assert t.day == 5
    assert t.hour == 1
    assert t.minute == 2
    assert t.second == 3
    assert t.tzinfo.utcoffset(None) == timedelta(hours=7, minutes=30)


def test_to_message(basic_feature):
    timestamp_field = basic_feature._data_type_definitions["Datetime"].data_type
    SilaTimestamp = timestamp_field.message_type

    msg = timestamp_field.to_message(
        datetime(
            year=2000,
            month=6,
            day=5,
            hour=1,
            minute=2,
            second=3,
            tzinfo=timezone(timedelta(hours=7, minutes=30)),
        )
    )

    assert isinstance(msg, SilaTimestamp)
    assert msg.year == 2000
    assert msg.month == 6
    assert msg.day == 5
    assert msg.hour == 1
    assert msg.minute == 2
    assert msg.second == 3
    assert msg.timezone.hours == 7
    assert msg.timezone.minutes == 30


def test_wrong_type(basic_feature):
    timestamp_field = basic_feature._data_type_definitions["Datetime"].data_type

    with pytest.raises(TypeError):
        # noinspection PyTypeChecker
        timestamp_field.to_message(3.14)


def test_seconds_in_timezone(basic_feature):
    timestamp_field = basic_feature._data_type_definitions["Datetime"].data_type

    with pytest.raises(ValueError):
        _ = timestamp_field.to_message(
            datetime(
                year=2000,
                month=6,
                day=5,
                hour=1,
                minute=2,
                second=3,
                tzinfo=timezone(timedelta(hours=7, minutes=30, seconds=10)),
            )
        )


def test_missing_timezone(basic_feature):
    timestamp_field = basic_feature._data_type_definitions["Datetime"].data_type

    with pytest.raises(ValueError):
        _ = timestamp_field.to_message(datetime(year=2000, month=6, day=5, hour=1, minute=2, second=3))


def test_from_string(basic_feature):
    timestamp_field = basic_feature._data_type_definitions["Datetime"].data_type

    assert timestamp_field.from_string("2000-01-01T23:59:00Z") == datetime(
        2000, 1, 1, 23, 59, tzinfo=timezone(timedelta(0))
    )
    assert timestamp_field.from_string("2010-12-31T23:59:13-02:30") == datetime(
        2010, 12, 31, 23, 59, 13, tzinfo=timezone(timedelta(hours=-2, minutes=-30))
    )
    with pytest.raises(ValueError):
        _ = timestamp_field.from_string("2010-12-31T23:59+02:30")
