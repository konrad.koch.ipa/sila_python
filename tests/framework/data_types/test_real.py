import pytest


def test_to_native_type(basic_feature):
    real_field = basic_feature._data_type_definitions["Float"].data_type
    SilaReal = real_field.message_type

    msg = SilaReal(value=10.0)
    val = real_field.to_native_type(msg)

    assert isinstance(val, float)
    assert val == 10.0


def test_to_message(basic_feature):
    real_field = basic_feature._data_type_definitions["Float"].data_type
    SilaReal = real_field.message_type

    msg = real_field.to_message(10.0)

    assert isinstance(msg, SilaReal)
    assert msg.value == 10.0


def test_wrong_type(basic_feature):
    real_field = basic_feature._data_type_definitions["Float"].data_type

    with pytest.raises(TypeError):
        # noinspection PyTypeChecker
        real_field.to_message("abc")


def test_overflow(basic_feature):
    real_field = basic_feature._data_type_definitions["Float"].data_type

    with pytest.raises(OverflowError):
        real_field.to_message(10 ** 1000)


def test_from_string(basic_feature):
    real_field = basic_feature._data_type_definitions["Float"].data_type

    assert real_field.from_string("1") == 1
    assert real_field.from_string("1.") == 1
    assert real_field.from_string("1.0") == 1
    assert real_field.from_string(".5") == 0.5
    assert real_field.from_string("-.5") == -0.5
    assert real_field.from_string("+.5") == 0.5

    with pytest.raises(ValueError):
        _ = real_field.from_string("1.1.1")
