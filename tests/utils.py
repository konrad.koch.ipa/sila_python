import random
import socket
from concurrent.futures import ThreadPoolExecutor
from os.path import dirname, join
from types import ModuleType
from typing import Any, Tuple

import grpc

resources_dir = join(dirname(__file__), "resources")


def get_feature_definition_str(feature_identifier: str) -> str:
    return open(get_fdl_path(feature_identifier)).read()


def get_fdl_path(feature_identifier: str) -> str:
    return join(resources_dir, "feature_definitions", feature_identifier + ".sila.xml")


def generate_port() -> int:
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        for port in random.sample(range(49152, 55000), 100):
            try:
                s.bind(("127.0.0.1", port))
                return port
            except OSError:  # port not free
                pass
    raise RuntimeError("Failed to find a free local port")


def create_server_stub(grpc_module: ModuleType, servicer) -> Tuple[grpc.Server, Any]:
    port = generate_port()

    server = grpc.server(ThreadPoolExecutor(max_workers=100, thread_name_prefix="test-grpc-server-executor"))
    add_func = getattr(
        grpc_module,
        [name for name in dir(grpc_module) if name.startswith("add_") and name.endswith("_to_server")][0],
    )
    add_func(servicer, server)
    server.add_insecure_port(f"localhost:{port}")
    server.start()

    stub_cls = getattr(grpc_module, [name for name in dir(grpc_module) if name.endswith("Stub")][0])
    stub = stub_cls(grpc.insecure_channel(f"localhost:{port}"))

    return server, stub
