# Translating SiLA data types to/from Python
The SiLA 2 standard defines data types independently of programming languages.
This document explains how this library handles SiLA 2 data types.

## Simple types
| SiLA | Python |
| ---- | ------ |
| [String](https://docs.google.com/document/d/1nGGEwbx45ZpKeKYH18VnNysREbr1EXH6FqlCo03yASM/edit#bookmark=id.2feq5qwf8if3) | [str](https://docs.python.org/3/library/stdtypes.html#str) (max length: 2^21) |
| [Integer](https://docs.google.com/document/d/1nGGEwbx45ZpKeKYH18VnNysREbr1EXH6FqlCo03yASM/edit#bookmark=id.5ke5vhtfebj1) | [int](https://docs.python.org/3/library/functions.html#int) (only values from -2^63 to 2^63-1) |
| [Real](https://docs.google.com/document/d/1nGGEwbx45ZpKeKYH18VnNysREbr1EXH6FqlCo03yASM/edit#bookmark=id.m0ym8ykqaw34) | [float](https://docs.python.org/3/library/functions.html#float) |
| [Boolean](https://docs.google.com/document/d/1nGGEwbx45ZpKeKYH18VnNysREbr1EXH6FqlCo03yASM/edit#bookmark=id.amiuggcddons) | [bool](https://docs.python.org/3/library/functions.html#bool) |
| [Binary](https://docs.google.com/document/d/1nGGEwbx45ZpKeKYH18VnNysREbr1EXH6FqlCo03yASM/edit#bookmark=id.ukjepbox2s3) | [bytes](https://docs.python.org/3/library/functions.html#func-bytes) (values > 2MB only work when server and client support [SiLA Binary Transfer](https://docs.google.com/document/d/1-shgqdYW4sgYIb5vWZ8xTwCUO_bqE13oBEX8rYY_SJA/edit#heading=h.ke82rexc7skp)) |
| [Date](https://docs.google.com/document/d/1nGGEwbx45ZpKeKYH18VnNysREbr1EXH6FqlCo03yASM/edit#bookmark=id.a6878c5vhz2y) | [tuple](https://docs.python.org/3/library/stdtypes.html#tuple) [[datetime.date](https://docs.python.org/3.9/library/datetime.html#datetime.date), [datetime.timezone](https://docs.python.org/3.9/library/datetime.html#datetime.timezone)]
| [Time](https://docs.google.com/document/d/1nGGEwbx45ZpKeKYH18VnNysREbr1EXH6FqlCo03yASM/edit#bookmark=id.ab3qdq99nf4z) | [datetime.time](https://docs.python.org/3.9/library/datetime.html#datetime.time) (requires explicit `tzinfo`, timezones are not optional in SiLA)
| [Timestamp](https://docs.google.com/document/d/1nGGEwbx45ZpKeKYH18VnNysREbr1EXH6FqlCo03yASM/edit#bookmark=id.uln1xwylmqhf) | [datetime.datetime](https://docs.python.org/3.9/library/datetime.html#datetime.datetime) (requires explicit `tzinfo`, timezones are not optional in SiLA)
| [Any](https://docs.google.com/document/d/1nGGEwbx45ZpKeKYH18VnNysREbr1EXH6FqlCo03yASM/edit#bookmark=id.doidi0mxh4q) | [tuple](https://docs.python.org/3/library/stdtypes.html#tuple) [[str](https://docs.python.org/3/library/stdtypes.html#str) (data type XML), [typing.Any](https://docs.python.org/3/library/typing.html#typing.Any) (value)], example: `("<DataType><Basic>Integer</Basic></DataType>", 123)` |
| [List](https://docs.google.com/document/d/1nGGEwbx45ZpKeKYH18VnNysREbr1EXH6FqlCo03yASM/edit#heading=h.cyniqf54mbgl) | [list](https://docs.python.org/3/library/stdtypes.html#list)

## [Structure](https://docs.google.com/document/d/1nGGEwbx45ZpKeKYH18VnNysREbr1EXH6FqlCo03yASM/edit#heading=h.yyd1j1hmtymp)
This library translates each structure into a [typing.NamedTuple](https://docs.python.org/3/library/typing.html#typing.NamedTuple), like this one:

```python
from typing import NamedTuple

class Point2D(NamedTuple):
    X: int
    Y: int
```

If you provide a plain tuple like `(1, 2)`, it will automatically be translated into the corresponding NamedTuple instance.

## [Constrained](https://docs.google.com/document/d/1nGGEwbx45ZpKeKYH18VnNysREbr1EXH6FqlCo03yASM/edit#heading=h.6j7v2yiba2p5)
Constrained types are translated into their base type, so a SiLA Constrained Integer is just a Python [int](https://docs.python.org/3/library/functions.html#int).
Constraints are checked automatically when a server receives constrained values.

## [Custom types](https://docs.google.com/document/d/1nGGEwbx45ZpKeKYH18VnNysREbr1EXH6FqlCo03yASM/edit#heading=h.txiicgij8k0z)
SiLA allows defining type aliases via `<DataTypeDefinition>` elements. These are translated into their basic type.
