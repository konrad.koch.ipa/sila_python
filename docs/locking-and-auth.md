# Locking and Authorization/Authentication features
SiLA defines core features for locking servers and user authorization/authentication.
The code generator can provide default implementations for you.

If you require different behavior, you can add these features like any other feature and implement them by yourself.
You can also use the described options to start with the default implementations and adapt them.

## [LockController](https://gitlab.com/SiLA2/sila_base/-/blob/master/feature_definitions/org/silastandard/core/LockController.sila.xml)
When generating a new SiLA server package (code generator command `new-package`), use the option `--lock-controller` to add a fully functional implementation of this feature to the server.

With the command `LockServer`, users can lock the server for a given duration. When a server is locked, a lock identifier has to be sent along with each command.

### Generate package, run server
These steps are explained [here](server-implementation.md), the only change is the option `--lock-controller`.
The GreetingProvider feature is again used as an example.

```shell
# generate package
$ python -m sila2.code_generator new-package -n my_server_package --lock-controller path/to/GreetingProvider.sila.xml
# implement the GreetingProvider feature
$ ...
# install package
$ pip install .
# run server on 127.0.0.1:50052
$ python -m my_server_package
```

### Interact with the server
```python-repl
>>> from sila2.client import SilaClient
>>> client = SilaClient("127.0.0.1", 50052)
>>> client.GreetingProvider.SayHello("World")  # works
>>> client.LockController.LockServer("my-lock-identifier", 60)  # lock server for a minute
>>> client.GreetingProvider.SayHello("World")  # error, missing lock identifier
>>> client.GreetingProvider.SayHello("World", metadata=[client.LockController.LockIdentifier("my-lock-identifier")])  # works
>>> client.GreetingProvider.SayHello("World", metadata=[client.LockController.LockIdentifier("other-lock-identifier")])  # error, invalid lock identifier
>>> client.LockController.UnlockServer("my-lock-identifier")  # unlock server
>>> client.GreetingProvider.SayHello("World")  # works
```

## [AuthenticationService](https://gitlab.com/SiLA2/sila_base/-/blob/master/feature_definitions/org/silastandard/core/AuthenticationService.sila.xml), [AuthorizationService](https://gitlab.com/SiLA2/sila_base/-/blob/master/feature_definitions/org/silastandard/core/AuthorizationService.sila.xml), [AuthorizationProviderService](https://gitlab.com/SiLA2/sila_base/-/blob/master/feature_definitions/org/silastandard/core/AuthorizationProviderService.sila.xml)
Use the code generator option `--auth-features` to add default implementations of these three core features.
They mostly work, but you should change the AuthenticationService implementation, which by default only allows the user `"admin"` to log in with the password `"admin"`.

AuthenticationService provides the commands `Login` and `Logout`:
- `Login(UserIdentification, Password, RequestedServer, RequestedFeatures) -> AccessToken, TokenLifetime`
  - The default implementation accepts only the username/password combination "admin"/"admin" and always uses a token lifetime of one hour
- `Logout(AccessToken)`

AuthorizationProviderService provides the command `Verify`:
- `Verify(AccessToken, RequestedServer, RequestedFeature) -> TokenLifetime`

AuthorizationService provides the metadata `AccessToken`.

### Generate the package
These steps are explained [here](server-implementation.md), the only change is the option `--auth-features`.
The GreetingProvider feature is again used as an example.

```shell
# generate package
$ python -m sila2.code_generator new-package -n my_server_package --auth-features path/to/GreetingProvider.sila.xml
# implement the GreetingProvider feature
$ ...
# (recommended) modify the AuthenticationService implementation to adapt the login mechanism
$ ...
# install package
$ pip install .
# run server on 127.0.0.1:50052
$ python -m my_server_package
```

### Interact with the server
```python-repl
>>> from sila2.client import SilaClient
>>> client = SilaClient("127.0.0.1", 50052)
>>> client.GreetingProvider.SayHello("World")  # error: missing access token
>>> server_uuid = client.SiLAService.ServerUUID.get()
>>> greetingprovider_fqi = client.GreetingProvider.fully_qualified_identifier
>>> login_response = client.AuthenticationService.Login("admin", "admin", server_uuid, [greetingprovider_fqi])  # request access to GreetingProvider
>>> token = login_response.AccessToken
>>> client.GreetingProvider.SayHello("World", metadata=[client.AuthorizationService.AccessToken(token)])  # works
>>> lifetime, = client.AuthorizationProviderService.Verify(token, server_uuid, greetingprovider_fqi)  # token lifetime in seconds
>>> client.GreetingProvider.SayHello("World", metadata=[client.AuthorizationService.AccessToken("another-token")])  # error: invalid token
>>> client.AuthenticationService.Logout(token)
>>> client.GreetingProvider.SayHello("World", metadata=[client.AuthorizationService.AccessToken(token)])  # error: token no longer valid
```
