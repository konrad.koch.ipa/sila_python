# Add and update features
During the development cycle of SiLA server applications, it can happen that you want to add new SiLA features or modify already implemented ones.

## Add new features
To add new features, use the `add-features` code generation command:
```shell
$ python -m sila2.code_generator add-features [-d package-directory] NewFeature1.sila.xml NewFeature2.sila.xml ...
```

Let's start by generating a new SiLA server package `my_sila_package` with no custom features.
```shell
$ python -m sila2.code_generator new-package -n my_sila_package
```

### Run code generator
Like in the basic example, we add the [GreetingProvider](https://gitlab.com/SiLA2/sila_base/-/blob/master/feature_definitions/org/silastandard/examples/GreetingProvider.sila.xml) feature.
```shell
$ python -m sila2.code_generator add-features path/to/GreetingProvider.sila.xml
```

### Implement feature
This has generated all relevant files, including `my_sila_package/feature_implementations/greetingprovider_impl.py`, where we now have to implement the commands and properties.
Open the file and replace both lines `raise NotImplementedError`.
- `SayHello`: `return f"Hallo {Name}"`
- `get_StartYear`: `return 2021`

### Add feature to server
In contrast to `new-package`, which adds features to the server automatically, with `add-features` you have to do that manually.
Open the file `my_sila_package/server.py`.

First, you need to import the generated feature and your implementation class:
- `from .generated.greetingprovider import GreetingProviderFeature`
- `from .feature_implementations.greetingprovider_impl import GreetingProviderImpl`

Inside the `__init__` method, add the following two lines:
- `self.greetingprovider_impl = GreetingProviderImpl()`
- `self.set_feature_implementation(GreetingProviderFeature, self.greetingprovider_impl)`

## Update feature
If you want to change features that already are part of a server package, use the `update` code generation command:
```shell
$ python -m sila2.code_generator update [-d package-directory]
```

### Change feature definition
For this example, let's modify the GreetingProvider feature by changing the command `SayHello(Name)` to `SayHelloWorld()`.
Open the file `my_sila_package/generated/greetingprovider/GreetingProvider.sila.xml` and change the `<Command>` element:
- change the `<Identifier>` from `SayHello` to `SayHelloWorld`
- change the `<DisplayName>` from `Say Hello` to `Say Hello World`
- change the `<Description>` to `Returns "Hello World"`
- remove the `<Parameter>` element

### Update the package
Run the following command:
```shell
$ python -m sila2.code_generator update
```

This regenerates the content of the directory `my_sila_package/generated` to match the contained feature definitions.

Additionally, it places new empty feature implementation files in the directory `my_sila_package/feature_implementations`, all with the prefix `updated_`.
To finish the update, merge the old and new implementation. In the end, the file `greetingprovider_impl.py` should contain a class `GreetingProviderImpl` with all method signatures like in `updated_greetingprovider_impl.py`.

In this case:
- copy `return 2021` from the original implementation to the updated file
- add `return "Hello World"` to the `SayHelloWorld` method in the updated file
- rename `updated_greetingprovider_impl.py` to `greetingprovider_impl.py` so it replaces the old implementation
