# Using SiLA clients
## Dynamic vs static clients
Dynamic clients can connect to any SiLA server without requiring previous information about its implemented SiLA features, so you do not need to generate any code to interact with SiLA servers.

The basic `sila2` library defines a class `sila2.client.SilaClient`, which implements a _dynamic_ SiLA client. You can use `client = `SilaClient(ip_address, port)` to connect to a server.

When using the code generator to generate a SiLA server application, a `SilaClient` subclass is generated. It can be imported via `from your_server_package import Client`, provides type annotations for features, and has more specific error handling (see below). 

## Examples
This repository contains an example server project, and multiple example client scripts.

## Connecting to a SiLA server
Note: Currently, this library only supports unencrypted connections.

### By IP address and port
If you know the IP address and port of the target server, you can instantiate a `SilaClient` object directly:
```python
from sila2.client import SilaClient

client = SilaClient("127.0.0.1", 50052)
```

### By SiLA Server Discovery
To start discovery, instantiate a `sila2.discovery.SilaDiscoveryBrowser` object. If you know the name or UUID of the target server (or just want to connect to the first available server), use the method `find_server`:
```python
from sila2.discovery import SilaDiscoveryBrowser

browser = SilaDiscoveryBrowser()
client = browser.find_server(server_name="MyServer", timeout=15)
client = browser.find_server(server_uuid="108a9f70-0cef-4dc4-a40e-a2fafbc63808", timeout=15)
client = browser.find_server()  # block until a server is found
```

SiLA Server Discovery currently only supports dynamic clients.

## Communication with SiLA servers
### Features
All features of a SiLA server are available as attributes of the `SilaClient`. The attribute names are the feature identifiers, e.g. "SiLAService" or "GreetingProvider".
So to interact with the [GreetingProvider](https://gitlab.com/SiLA2/sila_base/-/blob/master/feature_definitions/org/silastandard/examples/GreetingProvider.sila.xml) feature, use `client.GreetingProvider`, and to interact with the [SiLAService](https://gitlab.com/SiLA2/sila_base/-/blob/master/feature_definitions/org/silastandard/core/SiLAService.sila.xml) feature, use `client.SiLAService`.

### Unobservable properties
Unobservable properties are available as attributes of the feature object.
To request their values, use the `get()` method: `client.SiLAService.ImplementedFeatures.get()`.

This returns a list of strings: the fully qualified identifiers of the features implemented by the SiLA server.

### Unobservable commands
Unobservable commands are available as methods of the feature object: `client.SiLAService.SetServerName("MyServer")`.

SiLA commands can have multiple responses. This library translates these responses into [named tuples](https://docs.python.org/3/library/typing.html#typing.NamedTuple).

If a command has two responses called `Reponse1` and `Response2`, you can get the individual values in two ways:
```python
# unpacking
(response1, response2) = client.MyFeature.MyCommand()

# by name
response = client.MyFeature.MyCommand()
response1 = response.Response1
response2 = response.Response2
```

Parameters can be provided positional or keyword arguments (like with normal Python functions).
A command with three parameters `Param1` (String), `Param2` (Integer) and `Param3` (Boolean) could be executed like this:
```python
response = client.MyFeature.MyCommand("abc", 1, True)  # only positional
response = client.MyFeature.MyCommand("abc", Param2=1, Param3=True)  # mixed
response = client.MyFeature.MyCommand(Param3=True, Param1="abc", Param2=1)  # only by keyword
```

### Observable properties
Observable properties can be accessed like unobservable properties, but besides `get()`, they also have a `subscribe()` method.
It returns a stream of values. Streams are iterable and block until a new value is received. They provide a method `cancel()` to cancel the subscription.

```python
current_value = client.MyFeature.MyProperty.get()
value_stream = client.MyFeature.MyProperty.subscribe()

for value in value_stream:
    print("Value:", value)
    if value == 5:  # only useful if the property has the type Integer
        value_stream.cancel()
print("Subscription cancelled")
```

### Observable commands
Observable commands can be executed like unobservable commands: `client.MyFeature.MyCommand(param1, param2, ...)`.
But instead of returning the command response, the method returns a `ClientObservableCommandInstance` object.

This object has the following methods and attributes:
- `done`: Attribute, `True` if the execution finished
- `status`: Attribute, updated automatically
- `progress`: Attribute, updated automatically
- `estimated_remaining_time`: Attribute, updated automatically
- `subscribe_intermediate_responses()`: Subscribes to the intermediate responses, returns a stream
  - The steam works like observable property subscription streams
  - The intermediate responses are named tuples like command responses
- `get_responses()` Returns the command responses or raises a `CommandExecutionNotFinished` exception

### Metadata
To send SiLA Client Metadata along with a command execution or property access request, use the keyword argument `metadata`.
It is expected to be an iterable object (e.g. a list or tuple) of metadata instances.
You can create these by calling the metadata object with the intended value (`client.FeatureIdentifier.MetadataIdentifier(value)`).

```python
client.MyFeature.MyProperty.get(metadata=[client.MyFeature.MyMetadata("my_metadata_value")])
client.GreetingProvider.SayHello("World", metadata=[client.LockController.LockIdentifier("my-secret-token")])
```
