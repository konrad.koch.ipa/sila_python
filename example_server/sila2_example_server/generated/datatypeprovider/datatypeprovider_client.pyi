from __future__ import annotations

from typing import Any, Iterable, Optional

from datatypeprovider_types import ComplexCommand_Responses

from sila2.client import ClientMetadataInstance, ClientUnobservableProperty

from .datatypeprovider_types import IntegerAlias, StructureType

class DataTypeProviderClient:
    """
    Defines commands and properties to showcase handling of different data types
    """

    StructureProperty: ClientUnobservableProperty[Any]
    """
    A structure property
    """
    def ComplexCommand(
        self,
        Number: IntegerAlias,
        Structure: StructureType,
        *,
        metadata: Optional[Iterable[ClientMetadataInstance]] = None
    ) -> ComplexCommand_Responses:
        """
        A command with complex data types
        """
        ...
