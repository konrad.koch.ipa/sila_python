from __future__ import annotations

from typing import TYPE_CHECKING

from sila2.client import SilaClient

from .delayprovider import DelayProviderFeature, DelayTooLong
from .timerprovider import CountdownTooLong, TimerProviderFeature

if TYPE_CHECKING:

    from .datatypeprovider import DataTypeProviderClient
    from .delayprovider import DelayProviderClient
    from .greetingprovider import GreetingProviderClient
    from .timerprovider import TimerProviderClient


class Client(SilaClient):

    DataTypeProvider: DataTypeProviderClient

    DelayProvider: DelayProviderClient

    GreetingProvider: GreetingProviderClient

    TimerProvider: TimerProviderClient

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self._register_defined_execution_error_class(
            DelayProviderFeature.defined_execution_errors["DelayTooLong"], DelayTooLong
        )

        self._register_defined_execution_error_class(
            TimerProviderFeature.defined_execution_errors["CountdownTooLong"], CountdownTooLong
        )
