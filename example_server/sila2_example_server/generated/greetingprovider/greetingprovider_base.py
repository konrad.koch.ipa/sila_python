from __future__ import annotations

from abc import ABC, abstractmethod
from typing import Any, Dict

from sila2.framework import FullyQualifiedIdentifier
from sila2.server import FeatureImplementationBase

from .greetingprovider_types import SayHello_Responses


class GreetingProviderBase(FeatureImplementationBase, ABC):

    """
    Example implementation of a minimum Feature. Provides a Greeting to the Client
    and a StartYear property, informing about the year the Server has been started.
    """

    @abstractmethod
    def get_StartYear(self, *, metadata: Dict[FullyQualifiedIdentifier, Any]) -> int:
        """
        Returns the year the SiLA Server has been started in.

        :param metadata: The SiLA Client Metadata attached to the call
        :return: Returns the year the SiLA Server has been started in.
        """
        pass

    @abstractmethod
    def SayHello(self, Name: str, *, metadata: Dict[FullyQualifiedIdentifier, Any]) -> SayHello_Responses:
        """
        Does what it says: returns "Hello SiLA 2 + [Name]" to the client.


        :param Name: The name, SayHello shall use to greet.

        :param metadata: The SiLA Client Metadata attached to the call

        :return:

            - Greeting: The greeting string, returned to the SiLA Client.


        """
        pass
