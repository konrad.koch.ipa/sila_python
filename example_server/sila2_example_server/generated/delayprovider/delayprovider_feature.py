from os.path import dirname, join

from sila2.framework import Feature

DelayProviderFeature = Feature(open(join(dirname(__file__), "DelayProvider.sila.xml")).read())
