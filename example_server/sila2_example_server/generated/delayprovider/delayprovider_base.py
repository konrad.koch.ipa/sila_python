from __future__ import annotations

from abc import ABC, abstractmethod
from typing import Any, Dict, List, Union

from sila2.framework import Command, Feature, FullyQualifiedIdentifier, Property
from sila2.server import FeatureImplementationBase

from .delayprovider_types import Activate_Responses, Deactivate_Responses


class DelayProviderBase(FeatureImplementationBase, ABC):

    """
    Allows adding delay to calls via SiLA Client Metadata
    """

    @abstractmethod
    def get_IsActive(self, *, metadata: Dict[FullyQualifiedIdentifier, Any]) -> bool:
        """
        If active, some calls to the server will require the Delay metadata.

        :param metadata: The SiLA Client Metadata attached to the call
        :return: If active, some calls to the server will require the Delay metadata.
        """
        pass

    @abstractmethod
    def Activate(self, *, metadata: Dict[FullyQualifiedIdentifier, Any]) -> Activate_Responses:
        """
        Activate this feature.


        :param metadata: The SiLA Client Metadata attached to the call

        """
        pass

    @abstractmethod
    def Deactivate(self, *, metadata: Dict[FullyQualifiedIdentifier, Any]) -> Deactivate_Responses:
        """
        Deactivate this feature.


        :param metadata: The SiLA Client Metadata attached to the call

        """
        pass

    @abstractmethod
    def get_calls_affected_by_Delay(self) -> List[Union[Feature, Command, Property, FullyQualifiedIdentifier]]:
        """
          Returns the fully qualified identifiers of all features, commands and properties affected by the
          SiLA Client Metadata 'Delay'.

          **Description of 'Delay'**:
          When receiving this metadata in the context of a call, the server should wait for the specified duration
        before staring the requested execution

          :return: Fully qualified identifiers of all features, commands and properties affected by the
              SiLA Client Metadata 'Delay'.
        """
        pass
