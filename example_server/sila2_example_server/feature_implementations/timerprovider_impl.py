from __future__ import annotations

import datetime
import time
from concurrent.futures import Executor
from threading import Event
from typing import Any, Dict

from sila2_example_server.generated.timerprovider import CountdownTooLong

from sila2.framework import CommandExecutionStatus, FullyQualifiedIdentifier
from sila2.server import ObservableCommandInstanceWithIntermediateResponses

from ..generated.timerprovider import Countdown_IntermediateResponses, Countdown_Responses, TimerProviderBase


class TimerProviderImpl(TimerProviderBase):
    def __init__(self, executor: Executor):
        super().__init__()
        self.__stop_event = Event()

        def update_time(stop_event: Event):
            while not stop_event.is_set():
                self.update_CurrentTime(datetime.datetime.now(tz=datetime.timezone.utc).timetz())
                time.sleep(1)

        executor.submit(update_time, self.__stop_event)

    def Countdown(
        self,
        N: int,
        Message: str,
        *,
        metadata: Dict[FullyQualifiedIdentifier, Any],
        instance: ObservableCommandInstanceWithIntermediateResponses[Countdown_IntermediateResponses],
    ) -> Countdown_Responses:
        # send first info immediately
        instance.status = CommandExecutionStatus.running
        instance.progress = 0
        instance.estimated_remaining_time = datetime.timedelta(seconds=N)

        if N > 9000:
            raise CountdownTooLong

        for i in range(N, 0, -1):
            instance.send_intermediate_response(i)
            instance.progress = (N - i) / N * 100
            instance.estimated_remaining_time = datetime.timedelta(seconds=i)

            time.sleep(1)

        return Message, datetime.datetime.now(tz=datetime.timezone.utc)

    def stop(self):
        self.__stop_event.set()
