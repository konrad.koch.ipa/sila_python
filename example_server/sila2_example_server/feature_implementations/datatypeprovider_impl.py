import datetime
from typing import Any, Dict

from sila2_example_server.generated.datatypeprovider import ComplexCommand_Responses, DataTypeProviderBase
from sila2_example_server.generated.datatypeprovider.datatypeprovider_types import IntegerAlias, StructureType

from sila2.framework import FullyQualifiedIdentifier
from sila2.framework.data_types.date import SilaDateType


class DataTypeProviderImpl(DataTypeProviderBase):
    def get_StructureProperty(self, *, metadata: Dict[FullyQualifiedIdentifier, Any]) -> Any:
        return b"abc", [SilaDateType(datetime.date.today(), datetime.timezone.utc)]

    def ComplexCommand(
        self, Number: IntegerAlias, Structure: StructureType, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> ComplexCommand_Responses:
        assert isinstance(Number, int)

        assert isinstance(Structure, tuple)
        assert len(Structure) == 2
        l, b = Structure
        assert l is Structure.ListOfStrings
        assert b is Structure.Boolean

        assert isinstance(l, list)
        assert all(isinstance(item, str) for item in l)
        assert isinstance(b, bool)

        return (["a", "b", "c"], True), ("abc", 1.234)
