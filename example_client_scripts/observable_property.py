from sila2_example_server import Client


def main():
    client = Client("127.0.0.1", 50052)

    # requesting the current value of an observable property works like with unobservable properties:
    current_time = client.TimerProvider.CurrentTime.get()
    print("Current time:", current_time)

    # additionally, they allow subscription:
    current_time_subscription = client.TimerProvider.CurrentTime.subscribe()

    # subscription streams are iterable:
    received_times = 0
    for current_time in current_time_subscription:
        received_times += 1
        print("Current time from stream:", current_time)
        if received_times > 3:
            print("Breaking out of loop")
            break

    # note that the stream stays active until it is explicitly cancelled:
    current_time_subscription.cancel()


if __name__ == "__main__":
    main()
