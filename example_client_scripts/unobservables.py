from sila2_example_server import Client


def main():
    client = Client("127.0.0.1", 50052)

    # deactivate DelayProvider so this example is not affected by metadata
    client.DelayProvider.Deactivate()

    # command parameters can be specified as positional or keyword arguments:
    response1 = client.GreetingProvider.SayHello("World")
    response2 = client.GreetingProvider.SayHello(Name="World")
    assert response1 == response2

    # commands can have multiple responses, so they return a NamedTuple with one field per response.
    # the command GreetingProvider.SayHello has one response called 'Greeting':
    greeting1 = response1.Greeting

    # iterable unpacking also works:
    (greeting2,) = response2
    assert greeting1 == greeting2

    # properties cannot have multiple values, so they return the value directly:
    start_year = client.GreetingProvider.StartYear.get()

    print(f"Server says '{greeting1}' and was started in the year {start_year}")


if __name__ == "__main__":
    main()
