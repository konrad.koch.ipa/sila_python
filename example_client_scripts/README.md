# Client Examples
The programs in this directory all use the example server and demonstrate different aspects of working with SiLA Clients

## Implementation Notes
The class `SilaClient` implements a *dynamic* SiLA 2 Client. This means that is connects a server, requests the
definitions of all implemented features, and then serves as a client to these feature definitions.
This has the benefit of not requiring any feature-specific boilerplate code on the client-side.
However, this means that no code completion is available during development.

The planned code generator will generate type annotations to enable code completion and static analysis. Currently, the
class `ExampleClient` in the server module is written by hand.
All code would also work when using the `SilaClient` class directly.
