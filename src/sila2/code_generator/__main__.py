import sys

from sila2.code_generator import main

if __name__ == "__main__":
    sys.exit(main())
