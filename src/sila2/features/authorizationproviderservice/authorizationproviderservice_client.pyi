from __future__ import annotations

from typing import Iterable, Optional

from authorizationproviderservice_types import Verify_Responses

from sila2.client import ClientMetadataInstance

class AuthorizationProviderServiceClient:
    """
    This Feature provides SiLA Servers with the ability to check a given access token.
    """

    def Verify(
        self,
        AccessToken: str,
        RequestedServer: str,
        RequestedFeature: str,
        *,
        metadata: Optional[Iterable[ClientMetadataInstance]] = None
    ) -> Verify_Responses:
        """
        Verifies that a given token is valid for the requested server.
        """
        ...
