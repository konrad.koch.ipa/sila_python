from __future__ import annotations

from typing import NamedTuple


class LockServer_Responses(NamedTuple):

    pass


class UnlockServer_Responses(NamedTuple):

    pass
